#### lesta-course

###### built on:
- [![build on](https://github.com/hiebyshek/lesta-course/actions/workflows/build-ubuntu.yml/badge.svg)](https://github.com/hiebyshek/lesta-course/actions/workflows/build-ubuntu.yml)
- [![build on](https://github.com/hiebyshek/lesta-course/actions/workflows/build-macos.yml/badge.svg)](https://github.com/hiebyshek/lesta-course/actions/workflows/build-macos.yml)
- [![build on](https://github.com/hiebyshek/lesta-course/actions/workflows/build-windows.yml/badge.svg)](https://github.com/hiebyshek/lesta-course/actions/workflows/build-windows.yml)

###### to build docker:
    ./scripts/docker-build --target howmework-dir
    ./scripts/docker-build --help to see help page
